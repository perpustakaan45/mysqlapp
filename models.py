from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

class Customers(Base):
   __tablename__ = 'customers'
   userid = Column(Integer, primary_key =  True)
   username = Column(String)
   namaDepan = Column(String)
   namaBelakang = Column(String)
   email = Column(String)
