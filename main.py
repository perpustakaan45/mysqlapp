from models import Customers
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

class CustomersQuery:
    def __init__(self,db_user,db_password,port):
        engine = create_engine(f'mysql+mysqlconnector://{db_user}:{db_password}@localhost:{port}/perpustakaan')
        Session = sessionmaker(bind=engine)
        self.session = Session()

    def showUsers(self):
        res = self.session.query(Customers).all()
        return res

    def showUserById(self,**kwargs):
        res = self.session.query(Customers).filter(Customers.userid==kwargs['userid']).one()
        return res

    def insertUser(self,**kwargs):
        self.session.add(Customers(**kwargs))
        self.session.commit()

    def updateUserById(self,**kwargs):
        res = self.showUserById(userid=kwargs['userid'])
        for key,value in kwargs.items():
            if hasattr(res,key):
                setattr(res, key, value)
        self.session.commit()

    def deleteUserById(self,**kwargs):
        res = self.showUserById(userid=kwargs['userid'])
        self.session.delete(res)
        self.session.commit()

if __name__ == "__main__":
    perpustakaan = CustomersQuery('root','null','3306')
    #test read all
    a = perpustakaan.showUsers()
    for row in a:
        print(row.userid, row.username, row.namaDepan, row.namaBelakang, row.email)
    #test read by id
    b = perpustakaan.showUserById(userid=2)
    row = b
    print(row.userid, row.username, row.namaDepan, row.namaBelakang, row.email)
    
    #test insert
    user_data = {
        "userid":4,
        "username": "userkeempat",
        "namaDepan" : "Sangonomiya",
        "namaBelakang" : "Kokomi",
        "email" : "test@mail.com"
    }
    perpustakaan.insertUser(**user_data)
    c = perpustakaan.showUserById(userid=4)
    row = c
    print(row.userid, row.username, row.namaDepan, row.namaBelakang, row.email)
    #test update
    update_user_data = user_data
    update_user_data['email'] = "Test2@mail.com"
    print(update_user_data)
    perpustakaan.updateUserById(**update_user_data)
    d = perpustakaan.showUserById(userid=4)
    row = d
    print(row.userid, row.username, row.namaDepan, row.namaBelakang, row.email)
    #test delete
    perpustakaan.deleteUserById(**update_user_data)
    e = perpustakaan.showUsers()
    for row in e:
        print(row.userid, row.username, row.namaDepan, row.namaBelakang, row.email)
    
    

# if session:
#     print("Connection Success")
#     result =  session.query(Customers).all()
#     for row in result:
#         print(row.userid, row.username, row.namaDepan, row.namaBelakang, row.email)